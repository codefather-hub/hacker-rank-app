migrate:
	docker-compose up -d
	python3 -c "import time; time.sleep(3)"
	docker exec -it docker.web python3 /app/manage.py makemigrations core
	docker exec -it docker.web python3 /app/manage.py makemigrations
	docker exec -it docker.web python3 /app/manage.py migrate
	docker exec -it docker.web python3 /app/manage.py collectstatic --noinput
	docker-compose down

create_superuser:
	docker-compose up -d
	docker exec -it docker.web python3 /app/manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'admin')"
	docker-compose down

build:
	docker-compose build --no-cache
	make migrate
	make create_superuser

up:
	docker-compose up

db_drop:
	docker-compose down
	docker-compose rm postgres

prune:
	python3 docker/tools/prune.py

make freeze:
	pip3 freeze > docker/web/requirements.txt


push:
	git add .
	git commit -m "+autocommit"
	git push