# Hacker Rank Analogue #

Short documentation

### Quick Start ###

* git clone https://codefather-labs@bitbucket.org/codefather-hub/hacker-rank-app.git
* cd hacker-rank-app
* make build && make up

### Admin information ###

* login: admin
* password: admin

### Links ###

* http://0.0.0.0:8000/
* http://0.0.0.0:8000/admin/
* http://0.0.0.0:8000/swagger/
* http://0.0.0.0:8000/docs/


### Special a test task ###
