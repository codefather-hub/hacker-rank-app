import uuid

from django.db import models


class StatusMixin(models.Model):
    is_pending = models.BooleanField(
        default=True,
        blank=True,
        null=True,
        db_index=True,
        verbose_name='Ожидает'
    )

    is_done = models.BooleanField(
        default=False,
        blank=True,
        null=True,
        db_index=True,
        verbose_name='Завершена'
    )

    class Meta:
        abstract = True


class TimestampMixin(models.Model):
    created_at = models.DateTimeField(
        auto_now=True,
        null=True,
        blank=True
    )
    updated_at = models.DateTimeField(
        auto_now_add=True,
        null=True,
        blank=True
    )

    class Meta:
        abstract = True


class BaseModel(TimestampMixin):
    id = models.UUIDField(
        default=uuid.uuid4,
        editable=False,
        primary_key=True,
        db_index=True,
    )


class BaseTask(BaseModel, StatusMixin):
    class Meta:
        verbose_name = 'таск'
        verbose_name_plural = 'таски'

    def __str__(self):
        return f"{self.id}"


class CodeSolutionTask(BaseTask):
    solution = models.ForeignKey(
        'core.CodeSolution',
        default=None,
        null=True,
        blank=True,
        on_delete=models.deletion.SET_NULL,
    )

    class Meta:
        verbose_name = 'таск решения'
        verbose_name_plural = 'таски решения'

    def save(self, *args, **kwargs):
        if self.is_done:
            self.is_pending = False

            self.solution.is_done = True
            self.solution.save()
        return super(CodeSolutionTask, self).save(*args, **kwargs)


class CodeSolution(BaseModel, StatusMixin):
    class CodeResults(models.IntegerChoices):
        evaluation = 1
        correct = 2
        wrong = 3

    body = models.TextField(
        default=None,
        null=True,
        blank=True,
        db_index=True,
        verbose_name='Код'
    )

    result = models.PositiveIntegerField(
        choices=CodeResults.choices,
        default=CodeResults.evaluation.value,
        null=True,
        blank=False,
        verbose_name='Результат'
    )

    class Meta:
        verbose_name = 'решение'
        verbose_name_plural = 'решения'

    def __str__(self):
        return f"id: {self.id}"

    def save(self, *args, **kwargs):
        if self.is_done:
            self.is_pending = False
        return super(CodeSolution, self).save(*args, **kwargs)
