from django.contrib import admin

from apps.core.models import CodeSolution, CodeSolutionTask, BaseTask


class AdminSolutionMixin:
    date_hierarchy = 'created_at'
    empty_value_display = '-empty-'


@admin.register(CodeSolution)
class CodeSolutionAdmin(admin.ModelAdmin, AdminSolutionMixin):
    list_display = ('id', 'body', 'result', 'is_pending', 'is_done', 'created_at', 'updated_at')
    readonly_fields = ('id', 'body', 'result', 'is_pending', 'is_done', 'created_at', 'updated_at')
    list_filter = ('created_at', 'is_done', 'is_pending', 'result')

    change_form_template = 'root/admin/buttons.html'
    change_list_template = "root/admin/change_list.html"


@admin.register(CodeSolutionTask)
class CodeSolutionTaskAdmin(admin.ModelAdmin, AdminSolutionMixin):
    list_display = ('id', 'is_pending', 'is_done', 'created_at', 'updated_at')
    readonly_fields = ('id', 'solution', 'is_pending', 'is_done', 'created_at', 'updated_at')
    list_filter = ('created_at', 'is_done', 'is_pending', 'solution__result')

    change_form_template = 'root/admin/buttons.html'
    change_list_template = "root/admin/change_list.html"

