from rest_framework import serializers

from apps.core.models import CodeSolution


class CodeSolutionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CodeSolution
        fields = ['*']
