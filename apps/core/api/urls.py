from django.urls import path

from rest_framework.urlpatterns import format_suffix_patterns

from apps.core.api import views as api_views

urlpatterns = [
    path('code', api_views.create_code_solution, name='create-code-solution'),
    path('code/<str:task_id>/', api_views.read_code_solution, name='get-code-solution-result'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
