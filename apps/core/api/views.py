import json

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from apps.core.api.tasks import post_submission, result, app


def error_response(body: str):
    return json_response({
        "status": "error",
        "about": str(body)
    })


def json_response(body: dict):
    return json.loads(json.dumps(body))


@api_view(['POST'])
def create_code_solution(request) -> Response:
    """

    :param data: dict = {"text": "..."}
    :return: task_id: uuid
    """
    # Ключем кешировния решения вляется само решение
    # Если текущий ключ существует в кеше, результат таска вернется из кеша.
    # В противном случае, таск сходит в "API" и зашекирует результат по входному ключу.
    # Таким образом повторяющиеся решения не будут отправляться в апи.

    try:
        code = request.data['text']
        if code:
            return Response({"task_id": str(post_submission.delay(code))}, status=status.HTTP_200_OK)
        else:
            return Response(error_response("can't parse code"), status=status.HTTP_400_BAD_REQUEST)

    except KeyError:
        return Response(
            data=error_response("can't parse code"),
            status=status.HTTP_400_BAD_REQUEST
        )


@api_view(['GET'])
def read_code_solution(request, task_id: str, format=None) -> Response:
    task_result: result.AsyncResult = result.AsyncResult(id=task_id, app=app)
    if task_result:
        return Response(data={
            "result": task_result.result,
            "ready": task_result.ready(),
            "status": task_result.status
        }, status=status.HTTP_200_OK)
    return Response(data=error_response("can't parse task_id"), status=status.HTTP_200_OK)
