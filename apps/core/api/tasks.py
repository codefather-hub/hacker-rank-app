import random
import time
from typing import Tuple

from django.core.cache import cache
from celery import result

from settings.celery import app
from apps.core.models import CodeSolutionTask, CodeSolution


def get_submission(id: int) -> Tuple[int, str]:
    status = random.choice([*['evaluation'] * 10, 'correct', 'wrong'])

    return id, status


def wait_for(id: int) -> Tuple[int, str]:
    while True:
        result = get_submission(id)
        print(result)
        if result[1] != 'evaluation':
            return result


@app.task
def post_submission(reply: str) -> Tuple[int, str]:
    id = int(time.time() * 1000)
    cached = cache.get(reply)
    if not cached:
        result = wait_for(id)

        solution = CodeSolution.objects.create(body=reply, result=CodeSolution.CodeResults.evaluation)
        task = CodeSolutionTask.objects.create(solution=solution)

        task.is_done = True

        if result[1] == 'wrong':
            task.solution.result = CodeSolution.CodeResults.wrong.value

        elif result[1] == 'correct':
            task.solution.result = CodeSolution.CodeResults.correct.value

        task.solution.save()
        task.save()

        cache.set(reply, result)
        return result

    return cached
